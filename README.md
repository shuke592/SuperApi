<!--
 * @Author: 490912587@qq.com
 * @Date: 2023-12-20 10:44:09
 * @LastEditors: 490912587@qq.com
 * @LastEditTime: 2023-12-20 11:17:08
 * @FilePath: \SuperApi\README.md
 * @Description: 
-->
# SuperApi

#### 🎁框架介绍
无实体建库建表，对无实体库表进行增删改查，支持15种条件查询，以及分页，列表，无限级树形列表等等炫酷功能！
从此告别加班，告别知识付费，自己成为全栈大佬！
```
阅历痛点，相见恨晚！让 .NET 开发更简单，更通用，更流行！
```

#### 🍖纯净依赖
1. Panda.DynamicWebApi  动态API
2. SqlSugar  宇宙第一ORM没有之一
3. Swashbuckle.AspNetCore  Swagger自动构建接口文档


#### 🎖️安装教程
1.  VsStudio2023或者Rider打开SuperApi.sln
2.  NET8开发包
3.  SuperApi设为启动项启动

#### 🍁使用说明
1.  修改 SuperApi.Core/DataBase/Setup.cs的连接字符串
2.  修改 SuperApi.Core/Service/DynamicAppService.cs的连接字符串
3.  启动后浏览器会自动打开接口文档，即可放飞自我

#### 💐参与贡献
1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

#### 如需免费帮助服务请加群：526521167 SuperApi技术交流群，联系群主免费帮助
<img src="https://gitee.com/tmm-top/SuperApi/raw/master/doc/qun.png"/>

#### 🍎效果截图
<table>
    <tr>
        <td><img src="https://gitee.com/tmm-top/SuperApi/raw/master/doc/1.png"/></td>
        <td><img src="https://gitee.com/tmm-top/SuperApi/raw/master/doc/3.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/tmm-top/SuperApi/raw/master/doc/4.png"/></td>
        <td><img src="https://gitee.com/tmm-top/SuperApi/raw/master/doc/555.png"/></td>
    </tr>
</table>

## 🎀捐赠支持
```
如果对您有帮助，请点击右上角⭐Star关注或扫码捐赠，感谢支持开源！
```
<img src="https://gitee.com/tmm-top/SuperApi/raw/master/doc/zanshang.jpg"/>
